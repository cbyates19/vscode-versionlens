export * from './clients/dotnetClient.tests';
export * from './clients/nugetResourceClient.tests';
export * as DotNetUtils from './dotnetUtils/parseVersionSpec.tests';
export * as DotNetParser from './dotnetXmlParserFactory/createDependenciesFromXml.tests';