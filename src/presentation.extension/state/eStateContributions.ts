export enum StateContributions {
  Show = 'versionlens.show',
  PrereleasesEnabled = 'versionlens.prereleasesEnabled',
  ProviderActive = 'versionlens.providerActive',
  ProviderBusy = 'versionlens.providerBusy',
  ProviderError = 'versionlens.providerError',
  ProviderOpened = 'versionlens.providerOpened',
  ProviderSupportsPrereleases = 'versionlens.providerSupportsPrereleases',
}