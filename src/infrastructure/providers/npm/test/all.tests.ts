export * as GithubClient from './githubClient/fetchGitHub.tests';
export * as NpmPackageClient from './npmPackageClient/fetchPackage.tests';
export * as NpmSuggestionProvider from './npmSuggestionProvider/preFetchSuggestions.tests';
export * as NpmUtils from './npmUtils/all.tests';
export * as PacoteClient from './pacoteClient/all.tests';