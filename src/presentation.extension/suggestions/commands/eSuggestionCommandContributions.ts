export enum SuggestionCommandContributions {
  UpdateDependencyClicked = 'versionlens.suggestions.updateDependency',
  FileLinkClicked = "versionlens.suggestions.fileLinkDependency"
}