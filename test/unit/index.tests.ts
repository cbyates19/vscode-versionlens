export * as Application from 'test/unit/application/all.tests'
export * as Domain from 'test/unit/domain/all.tests'
export * as Infrastructure from 'test/unit/infrastructure/all.tests'